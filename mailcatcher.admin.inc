<?php

/**
 * Implements hook_form().
 */
function mailcatcher_admin_settings_form($form, &$form_state)
{
  $form['settings'] = array(
    '#title' => t('Outgoing emails settings'),
    '#type'  => 'fieldset',
    '#collapsible' => TRUE,
  );

  $form['settings']['mailcatcher_abort_all'] = array(
    '#title' => t('Abort all outgoing emails'),
    '#type' => 'checkbox',
    '#default_value' => variable_get('mailcatcher_abort_all'),
  );

  $form['settings']['mailcatcher_forward_all'] = array(
    '#title' => t('Forward all outgoing emails'),
    '#type' => 'checkbox',
    '#default_value' => variable_get('mailcatcher_forward_all'),
  );

  $form['settings']['mailcatcher_forward_address'] = array(
    '#title' => t('Recipient of forwarded emails'),
    '#type' => 'textfield',
    '#default_value' => variable_get('mailcatcher_forward_address'),
    '#description' => t('Enter multiple addresses separated by a comma'),
    '#size' => 120,
    '#states' => array(
      'invisible' => array(
        'input[name="mailcatcher_forward_all"]' => array('checked' => FALSE),
      ),
    ),
  );

  $form['settings']['mailcatcher_log_watchdog'] = array(
    '#title' => t('Catch outgoing emails to watchdog'),
    '#type' => 'checkbox',
    '#default_value' => variable_get('mailcatcher_log_watchdog'),
  );

  $form['settings']['mailcatcher_log_table'] = array(
    '#title' => t('Catch outgoing emails to a table'),
    '#type' => 'checkbox',
    '#default_value' => variable_get('mailcatcher_log_table'),
  );

  $form['settings']['mailcatcher_bcc'] = array(
    '#title' => t('Hidden recipient(s) of outgoing emails (Bcc)'),
    '#type' => 'textfield',
    '#default_value' => variable_get('mailcatcher_bcc'),
    '#description' => t('Enter multiple addresses separated by a comma'),
    '#size' => 120,
  );

  return system_settings_form($form);
}

/**
 * Form validation handler for mailcatcher_admin_settings_form().
 */
function mailcatcher_admin_settings_form_validate($form, &$form_state)
{
  $value = str_replace(';', ',', $form_state['values']['mailcatcher_bcc']);
  $split = array_filter(array_map('trim', explode(',', $value)));
  foreach ($split as $email) {
    if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
      form_set_error('mailcatcher_bcc', t('Incorrect email address'));
    }
  }
  $form_state['values']['mailcatcher_bcc'] = implode(',', $split);

  if (!empty($form_state['values']['mailcatcher_forward_all'])) {
    $value = str_replace(';', ',', $form_state['values']['mailcatcher_forward_address']);
    $split = array_filter(array_map('trim', explode(',', $value)));
    if (empty($split)) {
      form_set_error('mailcatcher_forward_address', t('Incorrect email address'));
    } else {
      foreach ($split as $email) {
        if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
          form_set_error('mailcatcher_forward_address', t('Incorrect email address'));
        }
      }
      $form_state['values']['mailcatcher_forward_address'] = implode(',', $split);
    }
  }
}

/**
 * Implements hook_form().
 */
function mailcatcher_admin_testmail_form($form, &$form_state)
{
  global $user;
  global $base_url;

  if (!isset($_SESSION['mailcatcher_id'])) {
    $_SESSION['mailcatcher_id'] = 1;
  }

  $form['email'] = array(
    '#title' => t('Recipient e-mail address'),
    '#type' => 'textfield',
    '#required'  => TRUE,
    '#default_value' => $user->mail,
  );

  $form['cc'] = array(
    '#title' => t('Send a copy to (Cc)'),
    '#type' => 'textfield',
  );

  $form['subject'] = array(
    '#title' => t('Subject'),
    '#type' => 'textfield',
    '#default_value' => t('Test mail #@id', array('@id' => $_SESSION['mailcatcher_id'])),
    '#required' => TRUE,
  );

  $form['body'] = array(
    '#title' => t('Body'),
    '#type' => 'textarea',
    '#default_value' => t("This is a test mail sent by @user on @date at @time from @base_url.\nIf you can read this message, it means the webserver is correctly configured to send e-mails.", array('@user' => $user->name, '@date' => date('d/m/Y'), '@time' => date('H:i'), '@base_url' => $base_url))
  );

  $form['actions'] = array(
    '#type' => 'actions',
    'submit' => array(
      '#type' => 'submit',
      '#value' => t('Send'),
    ),
  );

  return $form;
}

/**
 * Form validation handler for mailcatcher_admin_testmail_form().
 */
function mailcatcher_admin_testmail_form_validate($form, &$form_state)
{
  if (!filter_var($form_state['values']['email'], FILTER_VALIDATE_EMAIL)) {
    form_set_error('email', t('Incorrect email address'));
  }
}

/**
 * Form submission handler for mailcatcher_admin_testmail_form().
 */
function mailcatcher_admin_testmail_form_submit($form, &$form_state)
{
  global $language;

  $params = array(
    'subject' => $form_state['values']['subject'],
    'body' => $form_state['values']['body'],
    'cc' => $form_state['values']['cc'],
  );
  $message = drupal_mail('mailcatcher', 'mailcatcher_test', $form_state['values']['email'], $language, $params);
  if (!empty($message['result'])) {
    drupal_set_message(t('E-mail has been sent'), 'status');

    $_SESSION['mailcatcher_id']++;
  } else {
    drupal_set_message(t('Unable to send e-mail.'), 'error');
  }
}

/**
 * This function is assigned as a page callback in mymodalizy_admin_menu().
 *
 * @see mymodalizy_admin_menu()
 */
function mailcatcher_admin_table_page_callback()
{
  $form_state = array();
  $filter_form = drupal_build_form('mailcatcher_admin_table_form', $form_state);
  $results = mailcatcher_admin_table_load($form_state['values']['mail_key'], $form_state['values']['recipient'], $form_state['values']['subject'], $form_state['values']['body']);
  $rows = array();
  foreach ($results as $row) {

    $rows[] = array(
      $row->sent_date,
      $row->mail_key,
      $row->recipient,
      $row->subject,
      '<textarea readonly style="background-color:transparent;width:100%;">'.htmlentities($row->body).'</textarea>',
    );
  }

  return array(
    $filter_form,
    array(
      '#theme' => 'table',
      '#header' => array(t('Date'), t('Type'), t('To'), t('Subject'), t('Body')),
      '#rows' => $rows,
    ),
    array(
      '#theme' => 'pager',
      '#parameters' => array(
        'recipient' => $form_state['values']['recipient'],
        'subject' => $form_state['values']['subject'],
      ),
    ),
  );
}

/**
 * Implements hook_form().
 *
 * @see mailcatcher_admin_table_form_submit()
 */
function mailcatcher_admin_table_form($form, &$form_state)
{
  if ($form_state['submitted']) {
    $_GET['recipient'] = $form_state['values']['recipient'];
    $_GET['mail_key'] = $form_state['values']['mail_key'];
    $_GET['subject'] = $form_state['values']['subject'];
  }

  $form['fieldset'] = array(
    '#title' => t('Filter results'),
    '#type' => 'fieldset',
    '#attributes' => array(
      'class' => array('container-inline'),
    ),
  );
  $form['fieldset']['recipient'] = array(
    '#title' => t('Recipient'),
    '#type'  => 'textfield',
    '#size' => 20,
    '#default_value' => (isset($_GET['recipient'])) ? $_GET['recipient'] : '',
  );
  $form['fieldset']['mail_key'] = array(
    '#title' => t('Type'),
    '#type'  => 'textfield',
    '#size' => 20,
    '#default_value' => (isset($_GET['mail_key'])) ? $_GET['mail_key'] : '',
  );
  $form['fieldset']['subject'] = array(
    '#title' => t('Subject'),
    '#type'  => 'textfield',
    '#size' => 20,
    '#default_value' => (isset($_GET['subject'])) ? $_GET['subject'] : '',
  );
  $form['fieldset']['body'] = array(
    '#title' => t('Body'),
    '#type'  => 'textfield',
    '#size' => 20,
  );
  $form['fieldset']['submit'] = array(
    '#value' => t('Filter'),
    '#type'  => 'submit',
  );

  return $form;
}

/**
 * Form submission handler for mailcatcher_admin_table_form().
 */
function mailcatcher_admin_table_form_submit($form, &$form_state)
{
  $form_state['rebuild'] = TRUE;
}

/**
 * Load outgoing emails history
 *
 * @param  string $type
 * @param  string $to
 * @param  string $subject
 * @param  string $body
 * @return array
 */
function mailcatcher_admin_table_load($type = '', $to = '', $subject = '', $body = '')
{
  $query = db_select('mailcatcher', 'a')
    ->extend('PagerDefault')
    ->fields('a')
    ->limit(50)
    ->orderBy('id', 'DESC');

  if ($type !== '') {
    $query->condition('a.mail_key', $type, '=');
  }
  if ($to !== '') {
    $query->condition('a.recipient', '%'.$to.'%', 'LIKE');
  }
  if ($subject !== '') {
    $query->condition('a.subject', '%'.$subject.'%', 'LIKE');
  }
  if ($body !== '') {
    $query->condition('a.body', '%'.$body.'%', 'LIKE');
  }

  return $query->execute()->fetchAll();
}
